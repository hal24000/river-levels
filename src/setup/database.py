import pymongo
from pymongo import MongoClient


def db_con() -> pymongo.database.Database:
    """Connection to Mongo database."""
    db_uri = "mongodb://keishi_kohara:NZniFqa%60K32Uifb7VcoQ@10.0.1.4:27017,10.0.1.5:27017,10.0.1.6:27017/?authSource=cd_waterdemo&readPreference=primary&appname=MongoDB%20Compass&ssl=false"
    client = MongoClient(db_uri)
    db_con = client["cd_waterdemo"]
    return db_con
