import os
import sys
from typing import Tuple

import matplotlib
import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
from geopandas import GeoDataFrame
from tqdm import tqdm

module_path = os.path.abspath(os.path.join(".."))
sys.path.append(module_path + "/src")

from model.utils import get_npArray, store_npArray
from setup.database import db_con

db = db_con()
all_dates = pd.date_range(start="2017-01-01", end='2019-12-16', freq='1D')


class Covadem:
    """Covadem data and associated manipulations."""
    
    def load_covadem_data(self, date_time: pd._libs.tslibs.timestamps.Timestamp) -> pd.core.frame.DataFrame:
        """Load the COVADEM data into a geodataframe"""

        cov_df = pd.read_csv(
            '~/mgd2_rws_datalab/data/raw/out-rws-mi-{}/Werkendam-Lobith/{}-values.csv'.format(
                date_time.year,
                date_time.strftime('%Y-%-m-%-d')
            ),
            header=1)

        cov_df.rename(columns={
            'DateTime': 'datetime',
            'x': 'X',
            'y(EPSG:28992)': 'Y',
            'shipid(-)': 'ship_id',
            'hdop(-)': 'hdop',
            'ukc(meter)': 'ukc',
            'waterdepth(m)': 'depth'
        }, inplace=True)

        cov_df['geometry'] = cov_df.apply(lambda x: Point(x['X'], x['Y']), axis=1)
        cov_df = gpd.GeoDataFrame(cov_df, geometry='geometry')
        cov_df.crs = from_epsg(code='28992')

        cov_df['datetime'] = cov_df['datetime'].map(lambda x: x.strip('Z[UTC]'))
        cov_df.loc[cov_df['datetime'].map(lambda x: len(x) == 16), 'datetime'] = cov_df.loc[cov_df['datetime'].map(lambda x: len(x) == 16), 'datetime'].map(lambda x: x + ':00')
        cov_df['datetime'] = pd.to_datetime(cov_df['datetime'], format='%Y-%m-%dT%H:%M:%S')

        return cov_df

    @staticmethod
    def initialize_kalman():
        kal = KalmanFilter(dim_x=4, dim_z=2)

        kal.F = np.array([
            [1, 0, 1, 0],
            [0, 1, 0, 1],
            [0, 0, 1, 0],
            [0, 0, 0, 1]
        ])

        kal.H = np.array([
            [1, 0, 0, 0],
            [0, 1, 0, 0]
        ])

        kal.R *= 100 # lower the noise
        kal.R[2:, 2:] = 1000 # bigger uncetrainty on the velocity
        kal.P[2:, 2:] *= 1000
        kal.Q[2:, 2:] *= 0.05 # speed changes slowly
        
        return kal

    
    def run_kalman(self):
        timestep_limit = 3 #seconds
        for datum in tqdm(pd.date_range(start='2018-01-01', end='2019-12-31')):
            sys.stdout.write(f'\r{datum}')

            try:
                cov_df = load_covadem_data(datum)
            except:
                continue

            cov_df.sort_values(by='datetime', inplace=True)
            cov_df.reset_index(inplace=True, drop=True)

            cov_df['kalman_x'] = np.nan
            cov_df['kalman_y'] = np.nan

            for ship, grp in cov_df.groupby('ship_id'):
                if len(grp) < 3:
                    continue

                if (grp['datetime'].max() - grp['datetime'].min()).total_seconds() > timestep_limit * len(grp):
                    continue

                locs = grp[['X', 'Y']].values
                kal = initialize_kalman()
                kal.x = np.array(list(locs[0]) + list(locs[1] - locs[0]))

                kal_locs = [np.expand_dims(kal.x, axis=0)]
                for loc in locs[1:]:
                    kal.predict()
                    kal_locs.append(np.expand_dims(kal.x, axis=0))
                    kal.update(loc)

                kal_locs = np.concatenate(kal_locs, axis=0)

                cov_df.loc[grp.index, 'kalman_x'] = kal_locs[:,0]
                cov_df.loc[grp.index, 'kalman_y'] = kal_locs[:,1]

            cov_df.loc[cov_df['kalman_x'].isnull(), 'kalman_x'] = cov_df.loc[cov_df['kalman_x'].isnull(), 'X']
            cov_df.loc[cov_df['kalman_y'].isnull(), 'kalman_y'] = cov_df.loc[cov_df['kalman_y'].isnull(), 'Y']

            cov_df['kalman_geometry'] = cov_df.apply(lambda x: Point(x['kalman_x'], x['kalman_y']), axis=1)
            cov_df.set_geometry('kalman_geometry', inplace=True)
            cov_df.crs = from_epsg(code='28992')

            cov_df = gpd.sjoin(cov_df, reg_gdf, op='intersects', how='left')
            cov_df.dropna(inplace=True)

            tmp_df = []
            for q in [0., 0.01, 0.05, 0.25, 0.5, 0.75, 0.95, 0.99, 1.]:
                hlp_df = pd.DataFrame(cov_df.groupby('FID')['depth'].quantile(q))
                hlp_df.columns = ['percentile_{0:02d}'.format(int(100*q))]
                tmp_df.append(hlp_df)

            hlp_df = pd.DataFrame(cov_df.groupby('FID')['depth'].count())
            hlp_df.columns = ['n_measurements']
            tmp_df.append(hlp_df)

            cov_df = pd.concat(tmp_df, axis=1).reset_index()
            cov_df['datetime'] = datum

            if len(cov_df.to_dict("records"))>0:
                db['kk_covadem_100x50m_kalman_quantiles_and_numbers'].insert_many(cov_df.to_dict("records"))

            
    def train_ksvd(self):
        mgd_df = pd.read_pickle('/home/jovyan/river-levels/data/processed/big_biweekly_processed_shipping_canal.p.gz')
        mgd_df['datetime'] = pd.to_datetime(mgd_df['datetime'], format="%Y-%M-%D %H:%M:%S")
        mgd_df = mgd_df.sort_values("datetime").reset_index()

        no_regions = 3 - (mgd_df['region_id'].max()%3) + mgd_df['region_id'].max()
        full_index = list(range(no_regions))

        depth_list = []
        for i, (datum, grp) in tqdm(enumerate(mgd_df.groupby('datetime'))):
            sys.stdout.write(f"\rDoing {datum} ({i+1}/{mgd_df['datetime'].nunique()}).")

            tmp_df = grp.set_index('region_id').copy()
            tmp_df = tmp_df[~tmp_df.index.duplicated()]
            tmp_df = tmp_df.reindex(full_index)
            depths = tmp_df['min'].values.reshape(3,3,-1, order='F')
            depth_list.append(depths)

        depths = np.concatenate(depth_list, axis=2)
        depths = depths[:,:,np.isnan(depths).sum(axis=(0,1)) == 0]

        ksvd_train = np.transpose(depths.reshape(-1, depths.shape[2]))
        ksvd = MiniBatchDictionaryLearning(n_components=40)
        ksvd.fit(ksvd_train)

        with open('mgd2_ksvd.pkl', 'wb') as f:
            pickle.dump(ksvd, f)


    def run_ksvd(self):
        covadem_df = pd.DataFrame(db['kk_covadem_100x50m_kalman_quantiles_and_numbers'].find({}, {"_id":0}))
        covadem_df['FID'] = covadem_df['FID'].astype(int)
        with open('mgd2_ksvd.pkl', 'rb') as f:
            ksvd = pickle.load(f)

        no_regions = 2529
        full_index = list(range(no_regions))
        min_available_depths = 3 # how many measurements must be present in a 3x3 grid to fill in

        filled_in_list = []
        original_data = []
        fraction_df = []
        for datum in tqdm(all_dates):
            sys.stdout.write(f'\rDoing {datum}')
            frac_dict = {'datetime': datum}

            cov_df = covadem_df[covadem_df['datetime'] == datum]
            if cov_df.empty:
                filled_in_list.append(np.empty((3, 2529//3,1)))
                frac_dict['before_ksvd'] = 1.
                frac_dict['after_ksvd'] = 1.
                fraction_df.append(frac_dict)
                original = np.empty((3, 2529//3,1))
                original[:,:] = False
                original_data.append(original)
                continue

            cov_df = cov_df.set_index('FID')  # region ID
            cov_df.index = cov_df.index.astype(int)
            cov_df = cov_df.reindex(full_index)  # reindex full number of regiosn

            frac_dict['before_ksvd'] = cov_df['percentile_00'].isnull().sum() / len(cov_df)  # prior unfilled %

            original = ~np.isnan(cov_df['percentile_00'].values.reshape(3,-1, order='F'))
            original = np.expand_dims(original, axis=2)
            original_data.append(original)

            depths = cov_df['percentile_00'].values.reshape(3, 3, -1, order='F')
            depths_to_fill = np.transpose(depths.reshape(-1, depths.shape[2]))

            nan_mask = np.isnan(depths_to_fill)
            fin_mask = ~nan_mask
            depths_to_fill[nan_mask] = np.nanmedian(depths_to_fill)

            # ksvd
            filled_depths = ksvd.transform(depths_to_fill)
            filled_depths = np.dot(filled_depths, ksvd.components_)
            depths_to_fill[nan_mask] = filled_depths[nan_mask]
            depths_to_fill[~(fin_mask.sum(axis=1) >= min_available_depths), :] = np.nan

            frac_dict['after_ksvd'] = np.isnan(depths_to_fill).sum() / depths_to_fill.size
            fraction_df.append(frac_dict)

            depths_to_fill = np.transpose(depths_to_fill.reshape(-1,3,3, order='F').reshape(-1,3))
            depths_to_fill = np.expand_dims(depths_to_fill, axis=2)
            filled_in_list.append(depths_to_fill)

        fraction_df = pd.DataFrame(fraction_df)
        full_depths = np.concatenate(filled_in_list, axis=2)
        original_data = np.concatenate(original_data, axis=2)
        original_data = np.array(original_data, dtype=bool)
        
        store_npArray(full_depths, "kk_covadem_full_after_ksvd")
        db['kk_ksvd_fraction'].insert_many(fraction_df.to_dict('records'))
        store_npArray(original_data, "kk_covadem_ksvd_bool")
        
        return fraction_df, full_depths, original_data
    
    
    def plot_ksvd(self, region_id: int) -> matplotlib.figure.Figure:
        """Plots before and after ksvd levels for a single region.
        
        Args:
            region_id: Region ID number
        
        Returns:
            Figure showing before and after levels
        """
        # 240, 850
        original_data = get_npArray("61255f0c16af8920b7ce5379")
        full_depths = get_npArray("61255f4616af8920b7ce5385")
    
        fig, ax = plt.subplots(figsize=(20,8))
        row = region_id % 3
        col = region_id // 3

        ax.plot(all_dates[original_data[row,col,:]], full_depths[row,col, original_data[row,col, :]], marker='o', linestyle='none', label='Measured data')
        ax.plot(all_dates[~original_data[row,col,:]], full_depths[row,col, ~original_data[row,col, :]],
                marker='^', linestyle='none', label='KSVD filled in', markersize=15)
        ax.legend(fontsize=20)
        ax.set_ylabel('Depth (m)', fontsize=20)
        ax.set_title(f'Region {region_id}', fontsize=20)
        ax.tick_params(axis='both', which='major', labelsize=18)
        ax.tick_params(axis='both', which='minor', labelsize=18)
        
        return fig
    
    def get_ksvd_metrics(self) -> Tuple[np.float, np.float, matplotlib.figure.Figure]:
        """Calculates overall data coverage metrics and plot over time.
        
        Returns:
            Tuple containing before metric, after metric, and figure
        """
        fraction_df = pd.DataFrame(db['kk_ksvd_fraction'].find({}, {"_id":0}))
        before_ksvd = round(fraction_df['before_ksvd'].mean()*100,1)
        after_ksvd = round(fraction_df['after_ksvd'].mean()*100,1)
        
        fig, ax = plt.subplots(figsize=(20,8))
        fraction_df.set_index('datetime').plot(ax=ax, alpha=0.8, linewidth=3)
        ax.legend(fontsize=20)
        ax.set_xlabel('')
        ax.set_ylabel('Fraction of missing regions', fontsize=20)
        ax.tick_params(axis='both', which='major', labelsize=18)
        ax.tick_params(axis='both', which='minor', labelsize=18)
        
        return before_ksvd, after_ksvd, fig
        
        
        
class Training:
    def create_training_examples(
        self,
        data: np.array,
        distance: int = 4,
        lookback: int = 7,
        lookahead: int = 4,
    ) -> (np.array, np.array, list):
        """Create training examples for the neural net.

        Args:
            data: Preprocessed and filled in depth data (full_depths)
            distance: number of regions up- and downstream to look
            lookback: number of days to look back
            lookahead: period of the prediction

        Returns:
            a training example and the corrresponding target, plus metadata for the output (region ID for the triplet and the datetime)
        """
        all_dates = pd.date_range(start="2017-01-01", end='2019-12-16', freq='1D')
        
        features, targets, metadata = [], [], []
        cnt = 0
        for day in range(lookback, data.shape[2] - lookahead):  # 1080 days total. Range(7, 1073)
            for region in range(distance, data.shape[1] - distance):  # 843 columns of river total. Range(4, 839). "region triplet"
                sys.stdout.write(f'\r{cnt}')
                feat_array = data[:, region - distance: region + distance + 1, day - lookback + 1: day + 1]  # (3 rows, 0:9 regions/distance, 1:8 days)
                feat_array = np.expand_dims(feat_array, axis=0) # create a "channel"
                feat_array = np.expand_dims(feat_array, axis=0) # for concatenation, now (1, 1, 3, 9, 7)
                
                # using days 1-7 of data to predict days 8-11
                targ_array = data[:, region, day + 1: day + lookahead + 1].flatten().reshape(1,-1)  # (3 rows, region 4, 8:12 days), 4 days, flatten to (1,12)
                
                if np.isnan(targ_array).sum() == 0 and np.isnan(feat_array).sum() == 0:
                    features.append(feat_array)
                    targets.append(targ_array)
                    metadata.append({'datetime': all_dates[day], 'region_triplet': region})
                    cnt += 1

        print('Concatenating')
        # features list of 747695 lots of (1, 1, 3, 9, 7)
        features = np.concatenate(features, axis=0)  # (747695, 1, 3, 9, 7) 747695 features, 3 rows, 9 regions, 7 days
        targets = np.concatenate(targets, axis=0)  # (747695, 12) 747695 targets, 3 regions x 4 days = 12 results
        print('Concatenation complete')

        return features, targets, metadata
    
    @staticmethod
    def create_reg_gdf():
        """Creates region geodataframe containing region ID information and shipping region etc."""
        reg_gdf = pd.read_pickle('../data/processed/shipping_channel_100mx3_regions_with_metadata.p.gz', compression='gzip')
        reg_gdf = GeoDataFrame(reg_gdf, crs="EPSG:28992", geometry=reg_gdf["geometry"])
        reg_gdf['shipping_region'] = 'reg1'
        reg_gdf.loc[reg_gdf['waal_km'] > 887, 'shipping_region'] = 'reg2'
        reg_gdf.loc[reg_gdf['waal_km'] > 905, 'shipping_region'] = 'reg3'
        reg_gdf.loc[reg_gdf['waal_km'] > 914, 'shipping_region'] = 'reg4'
        reg_gdf.loc[reg_gdf['waal_km'] > 926, 'shipping_region'] = 'reg5'
        return reg_gdf
    
    @staticmethod
    def create_ship_df():
        """Ship counts by region and date."""
        ship_df = pd.read_excel('~/mgd2_rws_datalab/data/raw/ship_counts.xlsx', skiprows=4)
        ship_df.rename(columns={'Unnamed: 4': 'reg1',
                                'Unnamed: 5': 'reg2',
                                'Unnamed: 6': 'reg3',
                                'Unnamed: 7': 'reg4',
                                'Unnamed: 8': 'reg5'}, inplace=True)
        ship_df['date'] = pd.to_datetime(ship_df.apply(lambda x: '{}{}{}'.format(x['Dag (in maand)'],
                                                                                 x['Maand'],
                                                                                 x['Jaar']), axis=1),
                                         format='%d%B%Y')
        ship_df.drop(['Dag (in maand)', 'Maand', 'Jaar', 'TYPE'], axis=1, inplace=True)
        # melt table into single column
        ship_df = pd.melt(ship_df, id_vars='date', var_name='region', value_name='ship_counts')
        ship_df = ship_df.sort_values("date").reset_index(drop=True)
        db['kk_ship_counts'].insert_many(ship_df.to_dict("records"))
        
    def create_ship_filter(self, metadata, reg_gdf):
        """
        Created object ID: 61265c77f89733a1d46daf23
        """
        ship_df = pd.DataFrame(db['kk_ship_counts'].find({}, {"_id":0}))
        
        ship_counts = pd.DataFrame(metadata).copy()
        ship_counts['example_region'] = ship_counts['region_triplet'] * 3
        ship_counts = pd.merge(
            ship_counts,
            reg_gdf[['FID', 'shipping_region']],
            left_on='example_region',
            right_on='FID'
        )
        ship_counts.drop(['example_region', 'FID'], axis=1, inplace=True)
        ship_counts = pd.merge(
            ship_counts,
            ship_df,
            left_on=['datetime', 'shipping_region'],
            right_on=['date', 'region']
        )
        ship_counts.drop(['shipping_region', 'date', 'region'], axis=1, inplace=True)
        ship_counts = pd.pivot_table(ship_counts, index='datetime', columns='region_triplet', values='ship_counts')
        full_index = pd.date_range(start=ship_counts.index.min(), end=ship_counts.index.max())
        ship_counts = ship_counts.reindex(full_index)

        lookback = 42
        shipping_data = []
        for i, meta in tqdm(enumerate(metadata)):
            sys.stdout.write(f'\rDoing row {i+1}/{len(metadata)}')
            if meta['datetime'] >= pd.Timestamp('2019-10-13'):
                past_sh = np.empty((1, lookback, 1))
                past_sh[:] = np.nan
                shipping_data.append(past_sh)
                continue

            try:
                row = ship_counts.index.get_loc(meta['datetime'])
                col = meta['region_triplet']
                past_sh = ship_counts.iloc[row - lookback + 1 : row + 1][col].values.reshape(1,-1,1)
            except:
                past_sh = np.empty((1,lookback,1))
                past_sh[:] = np.nan

            if past_sh.shape != (1,lookback,1):
                past_sh = np.empty((1,lookback,1))
                past_sh[:] = np.nan

            shipping_data.append(past_sh)

        shipping_data = np.concatenate(shipping_data, axis=0)
        # sum across axis 1 and 2
        ship_filter = np.isnan(shipping_data).sum(axis=(1,2)) == 0
        
        store_npArray(ship_filter, "kk_ship_filter")