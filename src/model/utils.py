import os
import pickle
import sys

module_path = os.path.abspath(os.path.join(".."))
sys.path.append(module_path + "/src")

import gridfs
import numpy as np
from bson.binary import Binary
from bson.objectid import ObjectId

from setup.database import db_con


db = db_con()


def store_npArray(npArray: np.array, filename: str):
    """Takes numpy array and stores in MongoDB using fs chunking.
    
    Args:
        npArray: Numpy array
        filename: New file name
    """
    dataBSON = Binary(pickle.dumps(npArray, protocol=2), subtype=128)
    fs = gridfs.GridFS(db)
    object_id = fs.put(dataBSON, filename=filename)
    print(object_id)

    
def get_npArray(object_id: str) -> np.array:
    """Takes object id and returns np array
    
    Args:
        object_id: Object id of np array being retrieved
        
    Returns:
        Target numpy array
    """
    a = ObjectId(object_id)
    fs = gridfs.GridFS(db)
    npArray = pickle.loads(fs.get(a).read())
    return npArray