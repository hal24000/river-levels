# StUI

Basic template to create a Streamlit app for analysis, visualization and deployment via Dimension Dashboard.

## Usage
```bash
$ cd your_project_name/src
$ streamlit run app.py
```

## Project Organisation

    ├── chart                        <- Files required for Dimensionops build
    │
    ├── notebooks                    <- Jupyter notebooks. Naming convention is a number (for ordering),
    │                                   the creator's initials, and a short `-` delimited description, e.g.
    │                                   `1.0-KK-Initial-data-exploration`
    │
    ├── src                          <- Source code for project
    │   │
    │   │── model                    <- Model code
    │   │   └── plot.py              <- Visualization plots
    │   │
    │   │── setup                    <- Basic setup files
    │   │   │── database.py          <- Database connection
    │   │   │── favicon.ico          <- Icon
    │   │   └── layout.py            <- Layout settings
    │   │
    │   └── app.py                   <- App run file
    │   
    │── Dockerfile                   <- File to assemble a Docker image
    │
    │── environment.yml              <- Environment yml file to create conda environment
    │
    ├── README.md                    <- README for this project
    │
    └── requirements.txt             <- Requirements file for creating app environment